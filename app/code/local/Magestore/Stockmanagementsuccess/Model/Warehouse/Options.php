<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Stockmanagementsuccess
 * @copyright   Copyright (c) 2017 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

class Magestore_Stockmanagementsuccess_Model_Warehouse_Options
{

    /**
     * Return array of options as value-label pairs.
     *
     * @return array Format: array(array('value' => '<value>', 'label' => '<label>'), ...)
     */
    public function toOptionArray($locationId = null)
    {
        $mapCollection = Mage::getModel('inventorysuccess/warehouseLocationMap')->getCollection();
        if ($locationId) {
            $mapCollection->addFieldToFilter('location_id', array('neq' => $locationId));
        }
        $warehouseMapIDs = $mapCollection->getAllWarehouseIds();
        $wareHouseCollection = Mage::getModel('inventorysuccess/warehouse')->getCollection();
        if ($warehouseMapIDs) {
            $wareHouseCollection->addFieldToFilter('warehouse_id', array('nin' => $warehouseMapIDs));
        }
        $options = array(
            array('value' => 0, 'label' => Mage::helper('inventorysuccess')->__("Don't link to any warehouse")),
        );
        if (is_array($wareHouseCollection->toOptionArray())) {
            $options = array_merge($options, $wareHouseCollection->toOptionArray());
        }
        return $options;
    }

    /**
     * Return array of options as value-label pairs.
     *
     * @return array Format: array(array('value' => '<value>', 'label' => '<label>'), ...)
     */
    public function getAllOptionArray()
    {
        $options = array();
        $wareHouseCollection = Mage::getModel('inventorysuccess/warehouse')->getCollection();
        if(is_array($wareHouseCollection->getOptionArray())) {
            $options = array_merge($options, $wareHouseCollection->getOptionArray());
        }
        return $options;
    }

    /**
     * Return array of options as value-label pairs.
     *
     * @return array Format: array(array('value' => '<value>', 'label' => '<label>'), ...)
     */
    public function getWarehouseOptionArray()
    {
        $options = array(
             0 => Mage::helper('inventorysuccess')->__("Don't link to any warehouse"),
        );
        $wareHouseCollection = Mage::getModel('inventorysuccess/warehouse')->getCollection();
        if(is_array($wareHouseCollection->getOptionArray())) {
            $options = $options + $wareHouseCollection->getOptionArray();
        }
        return $options;
    }
    
}