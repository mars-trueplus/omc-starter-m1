<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Stockmanagementsuccess
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

class Magestore_Stockmanagementsuccess_Adminhtml_Stockmanagementsuccess_WarehouseController extends Mage_Adminhtml_Controller_Action
{
    /**
     * index action
     */
    public function indexAction()
    {
        $primaryWarehouse = Magestore_Coresuccess_Model_Service::warehouseService()->getPrimaryWarehouse();
        if($primaryWarehouse->getId()) {
            return $this->_redirect('adminhtml/inventorysuccess_warehouse/edit', array('id' => $primaryWarehouse->getId()));
        }
        return $this->_redirect('adminhtml/inventorysuccess_warehouse/index');
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin/inventorysuccess/stocklisting/warehouse_list');
    }

}